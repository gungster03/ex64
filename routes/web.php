<?php

use App\Http\Controllers\DishController;
use App\Http\Controllers\InstitutionController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\InstitutionController as AdminInstitutionsController;
use App\Http\Controllers\Admin\DishController as AdminDishesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InstitutionController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('institutions', InstitutionController::class)->only(['index', 'show']);
Route::resource('dishes', DishController::class)->only(['index', 'show']);
Route::post('dishes/addDish', [DishController::class, 'addDish'])->name('dishes.addDish');
Route::delete('dishes/removeDish', [DishController::class, 'removeDish'])->name('dishes.removeDish');


Route::prefix('admin')->name('admin.')->group(function (){
    Route::resources([
        'institutions' => AdminInstitutionsController::class,
        'dishes' => AdminDishesController::class,
    ]);
});
