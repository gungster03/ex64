<?php

namespace App\Http\Controllers;

use App\Models\Institution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InstitutionController extends Controller
{

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        if (Auth::user()){
            if (!session()->exists('institutions')){
                session()->put('institutions', []);
            }
        }
        $institutions = Institution::all();
        return view('institutions.index', compact('institutions'));
    }


    /**
     * @param Institution $institution
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Institution $institution)
    {
        return view('institutions.show', compact('institution'));
    }




}
