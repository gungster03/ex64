<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\InstitutionRequest;
use App\Models\Institution;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class InstitutionController extends Controller
{


    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', User::class);
        $institutions = Institution::all();
        return view('admin.institutions.index', compact('institutions'));
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $this->authorize('viewAny', User::class);
        return view('admin.institutions.create');
    }

    /**
     * @param InstitutionRequest $request
     * @return Application|Redirector|RedirectResponse
     */
    public function store(InstitutionRequest $request)
    {
        $this->authorize('viewAny', User::class);
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)){
            $path = $file->store('institutions', 'public');
            $data['picture']= $path;
            $data['picture_name'] = $request->file('picture')->getClientOriginalName();
        }
        $institution = new Institution($data);
        $institution->save();

        return redirect(route('admin.institutions.index'))->with('success',
            "Institution $institution->name has been successfully stored!");
    }

    /**
     * @param Institution $institution
     * @return Application|Factory|View
     */
    public function show(Institution $institution)
    {
        $this->authorize('viewAny', User::class);
        $institution->setRelation('dishes', $institution->dishes()->simplePaginate(2));
        return view('admin.institutions.show', compact('institution'));
    }

    /**
     * @param Institution $institution
     * @return Application|Factory|View
     */
    public function edit(Institution $institution)
    {
        $this->authorize('viewAny', User::class);
        return view('admin.institutions.edit', compact('institution'));
    }

    /**
     * @param InstitutionRequest $request
     * @param Institution $institution
     * @return Application|Redirector|RedirectResponse
     */
    public function update(InstitutionRequest $request, Institution $institution)
    {
        $this->authorize('viewAny', User::class);
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)){
            $path = $file->store('institutions', 'public');
            $data['picture']= $path;
            $data['picture_name'] = $request->file('picture')->getClientOriginalName();
        }
        $institution->update($data);

        return redirect(route('admin.institutions.show', compact('institution')))->with('success',
            "Institution $institution->name has been successfully updated!");
    }

    /**
     * @param Institution $institution
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(Institution $institution)
    {
        $this->authorize('viewAny', User::class);
        $institution->delete();
        return redirect(route('admin.institutions.index'))->with('success',
            "Establishment $institution->name has been successfully deleted!");
    }
}
