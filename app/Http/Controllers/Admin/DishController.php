<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DishRequest;
use App\Models\Dish;
use App\Models\Institution;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class DishController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $this->authorize('viewAny', User::class);
        $dishes = Dish::paginate(6);
        return view('admin.dishes.index', compact('dishes'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $this->authorize('viewAny', User::class);
        $institutions = Institution::all();
        return view('admin.dishes.create', compact('institutions'));
    }


    /**
     * @param DishRequest $request
     * @return Application|RedirectResponse|Redirector
     */
    public function store(DishRequest $request)
    {
        $this->authorize('viewAny', User::class);
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file))
        {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
            $data['picture_name'] = $request->file('picture')->getClientOriginalName();
        }

        $dish = new Dish($data);
        $dish->save();

        return redirect(route('admin.dishes.index'))->with('success',
            "Блюдо $dish->name успешно обновлено!");
    }


    /**
     * @param Dish $dish
     * @return Application|Factory|View
     */
    public function show(Dish $dish)
    {
        $this->authorize('viewAny', User::class);
        $institutions = Institution::all();
//        $dish->setRelation('establishment', $dish->establishment()->simplePaginate(2));
        return view('admin.dishes.show', compact('dish', 'institutions'));
    }


    /**
     * @param Dish $dish
     * @return Application|Factory|View
     */
    public function edit(Dish $dish)
    {
        $this->authorize('viewAny', User::class);
        $institutions = Institution::all();
        return view('admin.dishes.edit', compact('dish', 'institutions'));
    }


    /**
     * @param DishRequest $request
     * @param Dish $dish
     * @return Application|Redirector|RedirectResponse
     */
    public function update(DishRequest $request, Dish $dish)
    {
        $this->authorize('viewAny', User::class);
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file))
        {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
            $data['picture_name'] = $request->file('picture')->getClientOriginalName();
        }

        $dish->update($data);
        return redirect(route('admin.dishes.show', compact('dish')))->with('success',
            "Блюдо $dish->name успешно обновлено!");
    }


    /**
     * @param Dish $dish
     * @return Application|Redirector|RedirectResponse
     */
    public function destroy(Dish $dish)
    {
        $this->authorize('viewAny', User::class);
        $dish->delete();
        return redirect()->back()->with('success',
            "Блюдо $dish->name успешно удалено!");

    }
}
