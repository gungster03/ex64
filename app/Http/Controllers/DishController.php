<?php

namespace App\Http\Controllers;

use App\Models\Dish;
use Illuminate\Http\Request;

class DishController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth'])->only(['addDish']);
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $dishes = Dish::all();
        return view('dishes.index', compact('dishes'));
    }


    /**
     * @param Dish $dish
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Dish $dish)
    {
        return view('dishes.show', compact('dish'));
    }

    public function addDish(Request $request)
    {
        $dish = Dish::find($request->input('dish_id'));
        $institution = $dish->institution_id;
        if (session()->exists("institutions.$institution.$dish->id")){
            $count = session()->get("institutions.$institution.$dish->id.count") + 1;
            session()->put("institutions.$institution.$dish->id.count",$count );
            $sum = 0;
            foreach (session()->get("institutions.$institution") as $dish) {
                if (!is_int($dish)){
                    $total = $dish['price'] * $dish['count'];
                    $sum += $total;
                }
            }
            session()->put("institutions.$institution.sum", $sum );
        }else {
            session()->put("institutions.$institution.$dish->id");
            session()->put("institutions.$institution.$dish->id.id", $dish->id);
            session()->put("institutions.$institution.$dish->id.name", $dish->name);
            session()->put("institutions.$institution.$dish->id.price", $dish->price);
            session()->put("institutions.$institution.$dish->id.count", 1);
            $sum = 0;
            foreach (session()->get("institutions.$institution") as $dish) {
                if (!is_int($dish)){
                    $sum += $dish['price'] * $dish['count'];
                }
            }
            session()->put("institutions.$institution.sum", $sum );
        }
        return redirect()->back();

    }

    public function removeDish(Request $request)
    {
        $dish = Dish::find($request->input('dish_id'));
        $institution = $dish->institution_id;

        if (1 >= session()->get("institutions.$institution.$dish->id.count")) {
            $sum = session()->get("institutions.$institution.sum") - session()->get("institutions.$institution.$dish->id.price");
            session()->put("institutions.$institution.sum", $sum );
            session()->remove("institutions.$institution.$dish->id");
            return redirect()->back();
        }

        if (session()->get("institutions.$institution.$dish->id.count") > 1) {
            $sum = session()->get("institutions.$institution.sum") - session()->get("institutions.$institution.$dish->id.price");
            session()->put("institutions.$institution.sum", $sum );
            $count = session()->get("institutions.$institution.$dish->id.count") - 1;
            session()->put("institutions.$institution.$dish->id.count", $count);
            return redirect()->back();
        }
    }
}
