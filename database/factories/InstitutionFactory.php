<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class InstitutionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'content' => $this->faker->paragraph(10),
            'picture' => $this->getImage(rand(1,4)),
        ];
    }

    private function getImage(int $number = 1): string
    {
        $path = storage_path() . "/for_seed/" . $number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(300)->encode('jpg');
        Storage::disk('public')->put('institutions/'.$image_name, $resize->__toString());
        return 'dishes/'.$image_name;
    }
}
