<?php

namespace Database\Seeders;

use App\Models\Dish;
use App\Models\Institution;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(5)->create();
        $this->call(UserTableSeeder::class);
        Institution::factory(4)->create();
        Dish::factory(20)->create();
    }
}
