@extends('layouts.app')

@section('content')
    <div class="row mt-5">
        <div class="col-sm-3 m-3 basket-{{$dish->id}}">
            <div class="card" style="width: 18rem;">
                <img style="height: 300px; width: auto;" src="{{asset('/storage/' . $dish->picture)}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 id="basket-name" class="card-title">{{$dish->name}}</h5>
                    <h6>Цена: <span id="basket-price">{{$dish->price}}</span></h6>
                    <p class="card-text">{{$dish->content}}</p>
                </div>
            </div>
        </div>
    </div>
@endsection
