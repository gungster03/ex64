@extends('layouts.app')
@section('content')
    <h3 class="mt-5">Все блюда:</h3>
    <div class="row mt-5">
        @foreach($dishes as $dish)
            <div class="col-sm-3 m-3 basket-{{$dish->id}}">
                <div class="card" style="width: 18rem;">
                    <img style="height: 300px; width: auto;" src="{{asset('/storage/' . $dish->picture)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 id="basket-name" class="card-title">{{$dish->name}}</h5>
                        <h6>Цена: <span id="basket-price">{{$dish->price}}</span></h6>
                        <p class="card-text">{{$dish->content}}</p>
                        <form method="post" action="{{route('dishes.addDish')}}">
                            @csrf
                            <input type="hidden" id="" name="dish_id" value="{{$dish->id}}">
                            <button class="btn btn-primary">Заказать</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
