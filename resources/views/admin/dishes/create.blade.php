@extends('layouts.app')
@section('content')
    <div class="container">

        <form enctype="multipart/form-data" method="post" action="{{route('admin.dishes.store')}}">
            @csrf
            <div class="form-group row mt-5">
                <label for="name" class="col-sm-2 col-form-label ">Название блюда</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                           value="{{old('name')}}"/>
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="content" class="col-sm-2 col-form-label">Описание</label>
                <div class="col-sm-5">
                    <textarea class="form-control  @error('content') is-invalid @enderror" rows="10"  cols="30" name="content" id="content" >
                        {{old('content')}}
                    </textarea>
                    @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="price" class="col-sm-2 col-form-label">Цена</label>
                <div class="col-sm-5">
                    <input type="number" class="form-control  @error('price') is-invalid @enderror" name="price" id="price"/>
                    @error('price')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="establishment_id" class="col-sm-2 col-form-label">Название заведения</label>
                <div class="col-sm-5">
                    <select class="custom-select  @error('institution_id') is-invalid @enderror " name="institution_id">
                        @foreach($institutions as $institution)
                            <option value=""></option>
                            <option value="{{$institution->id}}">{{$institution->name}}</option>
                        @endforeach
                    </select>
                    @error('institution_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <label for="picture" class="col-sm-2 col-form-label">Фото</label>
                <div class="custom-file col-sm-5" >
                    <input type="file" class=" custom-file-input @error('picture') is-invalid @enderror " id="picture" name="picture">
                    <label class="custom-file-label" for="customFile">Выберите картинку</label>
                    @error('picture')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <hr class="border-dark-sm">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
@endsection


