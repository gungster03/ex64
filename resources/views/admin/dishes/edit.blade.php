@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Редактировать блюдо</h1>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <form enctype="multipart/form-data" method="post" action="{{route('admin.dishes.update',['dish' => $dish])}}">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="name">Наименование: </label>
                        <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" value="{{$dish->name}}" id="name">
                    </div>
                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="price">Цена: </label>
                        <input name="price" type="number" value="{{$dish->price}}" class="form-control @error('price') is-invalid @enderror" id="price">
                    </div>
                    @error('price')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group">
                        <label for="exampleInputPassword1">О блюде: </label>
                        <textarea name="content" class="form-control @error('content') is-invalid @enderror" id="exampleInputPassword1">{{$dish->content}}</textarea>
                    </div>
                    @error('content')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="form-group"><label class="col-sm-2 control-label">Заведение:</label>
                        <select class="custom-select" name="institution_id">
                            @foreach($institutions as $institution)
                                <option @if($dish->institution->id == $institution->id) selected @endif value="{{$institution->id}}">{{$institution->name}}</option>
                            @endforeach
                        </select>
                        @error('institution_id')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <div class="custom-file">
                            <img style="height: 150px; width: 150px;" src="{{asset('/storage/' . $dish->picture)}}" class="card-img-top" alt="{{$dish->picture_name}}">
                            <input type="file" value="{{$dish->picture}}" class="custom-file-input @error('picture') is-invalid @enderror " id="customFile" name="picture">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                            @error('picture')
                            <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                </form>
            </div>
        </div>
    </div>
@endsection


