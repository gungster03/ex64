@extends('layouts.app')
@section('content')

    <div class="container ">
        <div class="row mt-5">
            <div class="col">
                <a  class="btn btn-sm" href="{{route('admin.dishes.create')}}" style="background-color: burlywood; color: whitesmoke">
                    <h5>Добавить новое блюдо</h5>
                </a>
            </div>
        </div>
        <div class="dishes-block">
            <h5 class="pt-4 text-center" >Все блюда: </h5>
            <hr>
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3">
                @foreach($dishes as $dish)
                    <div class="col mb-4">
                        <div class="card">
                            <div class="card-header">
                                <img class="card-img-top" src="{{asset('/storage/'. $dish->picture)}}"
                                     alt="{{$dish->picture}}">
                            </div>
                            <div class="card-body">
                                <p class="text-truncate text-center text-capitalize"><b>{{ $dish->name}}</b></p>
                                <blockquote class="blockquote mb-0">
                                    <form class="btn btn-sm position-center" method="post" action="{{route('admin.dishes.destroy', ['dish'=> $dish])}}">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-sm btn-danger" type="submit">Удалить</button>
                                    </form>
                                    <a class="btn btn-sm" href="{{route('admin.dishes.edit', ['dish' => $dish])}}">
                                        <button class="btn btn btn-sm btn-primary" type="submit">Изменить</button>
                                    </a>
                                    <a class="btn btn-sm" href="{{route('admin.dishes.show', ['dish' => $dish])}}">
                                        <button class="btn btn btn-sm btn-success" type="submit">Подробнее</button>
                                    </a>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-auto">
                {{$dishes->links('pagination::bootstrap-4')}}
            </div>
        </div>
    </div>
@endsection

