@extends('layouts.app')
@section('content')
    <div class="container-sm">
        <div class="dish-info-block ">
            <h3 class="mb-4 text-danger text-center mt-5">{{$dish->name}}</h3>
            <div class="row ">
                <div class="col-12 col-md-8 pr-5 ">
                    <p class="mt-4 ml-2 mb-4 mr-2">{{$dish->content}}</p>
                </div>
                <div class="col-12 col-md-4">
                    <img src="{{asset('/storage/'. $dish->picture)}}"
                         alt="{{$dish->picture_name}}" class="w-30 h-auto mr-3">
                </div>
            </div><br>
            <div class="row">
                <div class="col-12 col-md-8 pr-5 ">
                    <h5>Заведение:</h5>
                    <a href="{{route('admin.institutions.show', ['institution'=> $dish->institution->id])}}">
                        <p>
                            <b>{{$dish->institution->name}}</b>
                        </p>
                    </a>
                </div>
            </div>
        </div>
        <hr>
    </div>
@endsection



