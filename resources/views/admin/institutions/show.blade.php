@extends('layouts.app')
@section('content')
    <div class="container-sm">
        <div class="institution-block">
            <h3 class="mb-4 mt-4 text-danger text-center">{{$institution->name}}</h3>
            <div class="row ">
                <div class="col-12 col-md-6 pr-5 ">
                    <p class="mt-4 ml-2 mb-4 mr-2 text-center">{{$institution->content}}</p>
                </div>
                <div class="col-12 col-md-6">
                    <img src="{{asset('/storage/'. $institution->picture)}}"
                         alt="{{$institution->picture_name}}" class="w-30 h-50 mr-3">
                </div>
            </div>
        </div>
        <hr>
        <div class="container-sm">
            <h3 class="mb-5 mt-5 text-center">Блюда</h3>
            @foreach($institution->dishes as $dish)
                <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 justify-content-center pb-5">
                    <div class="col pr-5">
                        <a href="{{route('admin.dishes.show', ['dish'=> $dish])}}">
                            <p><b>{{$dish->name}}</b></p>
                        </a>
                    </div>
                    <div class="col">
                        <p>{{$dish->content}}</p>
                    </div>
                    <div class="col">
                        <img src="{{asset('/storage/'.$dish->picture)}}" alt="{{$dish->picture_name}}">
                    </div>
                </div>
            @endforeach
            <div class="row">
                <div class="col-md-auto">
                    {{$institution->dishes->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection





