@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-4 mb-3">
            <h1>Добавить новое заведении</h1>
        </div>
        <form enctype="multipart/form-data" method="post" action="{{route('admin.institutions.store')}}">
            @csrf
            <div class="mb-3">
                <label for="name">Название заведения: </label>
                <input type="text" class="form-control  @error('name') is-invalid @enderror" id="name" name="name" value=" {{old('name')}}">
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="content">Подробнее о заведении: </label>
                <textarea class="form-control  @error('content') is-invalid @enderror" rows="10" cols="30"
                          name="content" id="content" >
                        {{old('content')}}
                    </textarea>
                @error('content')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="mb-3">
                <label for="picture" class=" col-form-label">Фото: </label>
                <div class="custom-file form-control" >
                    <input type="file" class="custom-file-input @error('picture') is-invalid @enderror "
                           id="picture" name="picture" value="{{old('picture')}}">
                    <label class="custom-file-label" for="customFile">Выберите фотографию</label>
                    @error('picture')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <hr class="border-dark-sm">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
    </div>
@endsection




