@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row pt-5">
            <div class="col">
                <a  class="btn btn-sm" href="{{route('admin.institutions.create')}}"
                    style="background-color: burlywood; color: whitesmoke">
                    <h5>Добавить новое заведение</h5>
                </a>
            </div>
        </div>
        <div class="container-sm mt-5">
            <table class="table">
                <thead style="color: #5d561b">
                <tr>
                    <th scope="col"><h4>Наименование заведений</h4></th>
                    <th scope="col"><h4>Действие</h4></th>
                </tr>
                </thead>
                <tbody>
                @foreach($institutions as $institution)
                    <tr>
                        <th scope="row">
                            <a style="color: #4c110f" href="{{route('admin.institutions.show',['institution'=> $institution])}}">
                                {{$institution->name}}
                            </a>
                        </th>
                        <td>
                            <a class="btn btn-sm" href="{{route('admin.institutions.edit', ['institution' => $institution])}}">
                                <button class="btn btn btn-sm btn-primary" type="submit">Update</button>
                            </a>
                            <a class="btn btn-sm" >
                                <form method="POST" action="{{route('admin.institutions.destroy', ['institution'=> $institution])}}">
                                    @method('delete')
                                    @csrf
                                    <button class="btn btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
