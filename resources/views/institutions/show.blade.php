@extends('layouts.app')
@section('content')
    <h3 class="mt-5">Все блюда в заведении {{$institution->name}}:</h3>
    <div class="row">
        @foreach($institution->dishes as $dish)
            <div class="col-sm-3 m-3 basket-{{$dish->id}}">
                <div class="card" style="width: 18rem;">
                    <img style="height: 300px; width: auto;" src="{{asset('/storage/' . $dish->picture)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 id="basket-name" class="card-title">{{$dish->name}}</h5>
                        <h6>Цена: <span id="basket-price">{{$dish->price}}</span></h6>
                        <p class="card-text">{{$dish->content}}</p>
                        <form method="post" action="{{route('dishes.addDish')}}">
                            @csrf
                            <input type="hidden" id="" name="dish_id" value="{{$dish->id}}">
                            <button class="btn btn-primary">Заказать</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    @if(session()->exists("institutions.$institution->id"))
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    @foreach(session()->get("institutions.$institution->id") as $dish)
                        @if(!is_int($dish))
                            <p><b>Блюдо: {{$dish['name']}}</b></p>
                            <p>
                            <form method="post" action="{{route('dishes.removeDish')}}">
                                @csrf
                                @method('delete')
                                <input type="hidden" id="" name="dish_id" value="{{$dish['id']}}">
                                <button class="btn btn-sm btn-primary">-</button>
                            </form>
                                <b>Количество: {{$dish['count']}}</b>
                                  <form method="post" action="{{route('dishes.addDish')}}">
                                    @csrf
                                    <input type="hidden" id="" name="dish_id" value="{{$dish['id']}}">
                                    <button class="btn btn-sm btn-primary">+</button>
                                </form>
                            </p>
                            <hr>
                        @endif
                    @endforeach
                    <p><b>Общаяя сумма: {{session()->get("institutions.$institution->id.sum")}}</b></p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
