@extends('layouts.app')
@section('content')
    <h3 class="mt-5">Наши заведения:</h3>
    <div class="row mt-5">
        @foreach($institutions as $institution)
            <div class="col-sm-3 m-3">
                <div class="card" style="width: 18rem;">
                    <img style="height: 300px; width: auto;" src="{{asset('/storage/' . $institution->picture)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">{{$institution->name}}</h5>
                        <p class="card-text">{{$institution->content}}</p>
                        <a href="{{route('institutions.show', ['institution' => $institution])}}" class="btn btn-primary">Зайти</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    @if(session()->exists("institutions.$institution->id"))
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        @foreach(session()->get("institutions.$institution->id") as $dish)
                            @if(!is_int($dish))
                                <p><b>Блюдо: {{$dish['name']}}</b></p>
                                <p><b>Количество: {{$dish['count']}}</b></p>
                                <hr>
                            @endif
                        @endforeach
                        <p><b>Общаяя сумма: {{session()->get("institutions.$institution->id.sum")}}</b></p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
