<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;
    protected $seed = true;


    /**
     * @group user
     */
    public function test_auth_get_dish()
    {
        $user = \App\Models\User::factory()->create();
        $response = $this->actingAs($user)->get('/login');
        $response = $this->get(route('dishes.index'));
        $response->assertSee('₽');
    }

    /**
     * @group user
     */
    public function test_not_auth_user_delete_dish()
    {

        $response = $this->delete(route('admin.dishes.destroy', ['dish' => \App\Models\Dish::first()]));
        $response->assertStatus(403);
    }

    /**
     * @group user
     */
    public function test_not_auth_user_admin_create_dish()
    {
        $response = $this->get(route('admin.dishes.create'));
        $response->assertStatus(403);
    }

    /**
     * @group user
     */
    public function test_not_auth_user_get_admin_dish_index()
    {
        $response = $this->get(route('admin.dishes.index'));
        $response->assertStatus(403);
    }

    /**
     * @group user
     */
    public function test_auth_user_get_admin_dish_index()
    {
        $user = \App\Models\User::factory()->create();
        $user->is_admin = true;
        $user->save();
        $response = $this->actingAs($user)->get('/login');
        $response = $this->get(route('admin.dishes.index'));
        $response->assertStatus(200);
    }

    /**
     * @group user
     */
    public function test_auth_admin_delete_dish()
    {
        $user = \App\Models\User::factory()->create();
        $user->is_admin = true;
        $user->save();
        $response = $this->actingAs($user)->get('/login');
        $response = $this->delete(route('admin.dishes.destroy', ['dish' => \App\Models\Dish::first()]));
        $response->assertStatus(302);
    }
}
